# BlackJack

## Build and Run

This project requires JDK 11 or higher to be built and run.

1. Run tests and build:
`./gradlew clean build`

2. Run game:
`java -jar build/libs/blackjack.jar`

