plugins {
    java
}

group = "pw.spn"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

val jar by tasks.getting(Jar::class) {
    archiveName = "blackjack.jar"
    manifest {
        attributes["Main-Class"] = "pw.spn.blackjack.BlackJackApplication"
    }
}
