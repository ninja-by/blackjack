package pw.spn.blackjack;

import pw.spn.blackjack.game.impl.ConsoleBlackJack;

public final class BlackJackApplication {
    public static void main(String[] args) {
        new ConsoleBlackJack().start();
    }
}
