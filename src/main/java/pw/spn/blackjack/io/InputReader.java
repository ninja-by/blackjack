package pw.spn.blackjack.io;

@FunctionalInterface
public interface InputReader {
    int nextChar();
}
