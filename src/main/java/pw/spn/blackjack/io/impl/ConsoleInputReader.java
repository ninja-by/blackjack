package pw.spn.blackjack.io.impl;

import pw.spn.blackjack.io.InputReader;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public final class ConsoleInputReader implements InputReader {
    private final Scanner input = new Scanner(System.in, StandardCharsets.UTF_8);

    @Override
    public int nextChar() {
        try {
            return input.nextLine().charAt(0);
        } catch (Exception e) {
            System.out.println("Invalid command");
        }
        return 0;
    }
}
