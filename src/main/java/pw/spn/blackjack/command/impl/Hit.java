package pw.spn.blackjack.command.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public final class Hit implements Command {
    @Override
    public String getName() {
        return "Hit";
    }

    @Override
    public char getShortcut() {
        return 'h';
    }

    @Override
    public Consumer<Player> action() {
        return Player::hit;
    }
}
