package pw.spn.blackjack.command.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public final class Split implements Command {
    @Override
    public String getName() {
        return "Split";
    }

    @Override
    public char getShortcut() {
        return 'p';
    }

    @Override
    public Consumer<Player> action() {
        return Player::split;
    }
}
