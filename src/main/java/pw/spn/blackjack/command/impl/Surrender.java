package pw.spn.blackjack.command.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public final class Surrender implements Command {
    @Override
    public String getName() {
        return "Surrender";
    }

    @Override
    public char getShortcut() {
        return 'r';
    }

    @Override
    public Consumer<Player> action() {
        return Player::surrender;
    }
}
