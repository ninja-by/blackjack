package pw.spn.blackjack.command.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public final class DoubleDown implements Command {
    @Override
    public String getName() {
        return "Double Down";
    }

    @Override
    public char getShortcut() {
        return 'd';
    }

    @Override
    public Consumer<Player> action() {
        return Player::doubleDown;
    }
}
