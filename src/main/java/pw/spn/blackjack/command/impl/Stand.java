package pw.spn.blackjack.command.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public final class Stand implements Command {
    @Override
    public String getName() {
        return "Stand";
    }

    @Override
    public char getShortcut() {
        return 's';
    }

    @Override
    public Consumer<Player> action() {
        return Player::stand;
    }
}
