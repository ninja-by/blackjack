package pw.spn.blackjack.command;

import pw.spn.blackjack.player.Player;

import java.util.function.Consumer;

public interface Command {
    String getName();

    char getShortcut();

    Consumer<Player> action();
}
