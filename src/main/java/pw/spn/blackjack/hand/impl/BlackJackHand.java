package pw.spn.blackjack.hand.impl;

import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.hand.Hand;

import java.util.*;
import java.util.stream.Stream;

import static pw.spn.blackjack.card.Face.*;

public final class BlackJackHand implements Hand {
    private static final int TARGET_VALUE = 21;

    private final Map<Face, Integer> numberValues = new EnumMap<>(Face.class);

    private final List<Card> cards = new ArrayList<>();
    private int numberOfAces;
    private int valueWithoutAces;
    private boolean isFinished;

    public BlackJackHand() {
        Face[] numberFaces = {TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE};
        for (int i = 0; i < numberFaces.length; i++) {
            numberValues.put(numberFaces[i], i + 2);
        }
        Stream.of(TEN, JACK, QUEEN, KING).forEach(face -> numberValues.put(face, 10));
    }

    @Override
    public void addCard(Card card) {
        Objects.requireNonNull(card);

        cards.add(card);
        Face cardFace = card.getFace();
        if (cardFace == ACE) {
            numberOfAces++;
        } else {
            valueWithoutAces += numberValues.get(cardFace);
        }
    }

    @Override
    public int getValue() {
        // no Aces, just return value
        if (numberOfAces == 0) {
            return valueWithoutAces;
        }

        // if current value is higher than 10 we can't add any Ace as 11
        if (valueWithoutAces > 10) {
            return valueWithoutAces + numberOfAces;
        }

        //current value is less than 10
        int resultValue = valueWithoutAces + 11;

        // we can add one Ace as 11
        if (numberOfAces == 1) {
            return resultValue;
        }

        // we have more than one Ace, need to check if need to recalculate first Ace
        if (TARGET_VALUE - resultValue < numberOfAces - 1) {
            resultValue -= 10;
        }

        return resultValue + numberOfAces - 1;
    }

    @Override
    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    @Override
    public void stand() {
        isFinished = true;
    }

    @Override
    public Hand[] split() {
        Hand first = new BlackJackHand();
        Hand second = new BlackJackHand();
        first.addCard(cards.get(0));
        second.addCard(cards.get(1));
        return new Hand[]{first, second};
    }

    @Override
    public boolean canHit() {
        return !isFinished && getValue() < TARGET_VALUE;
    }

    @Override
    public String toString() {
        return cards + " (" + getValue() + ")";
    }
}
