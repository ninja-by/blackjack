package pw.spn.blackjack.hand;

import pw.spn.blackjack.card.Card;

import java.util.List;

public interface Hand {
    void addCard(Card card);

    int getValue();

    boolean canHit();

    List<Card> getCards();

    void stand();

    Hand[] split();
}
