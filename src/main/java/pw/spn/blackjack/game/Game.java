package pw.spn.blackjack.game;

import pw.spn.blackjack.deck.Deck;

public interface Game {
    void start();

    int getBet();

    Deck getDeck();
}
