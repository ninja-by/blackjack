package pw.spn.blackjack.game.impl;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.command.impl.DoubleDown;
import pw.spn.blackjack.command.impl.Surrender;
import pw.spn.blackjack.dealer.Dealer;
import pw.spn.blackjack.dealer.impl.BlackJackDealer;
import pw.spn.blackjack.deck.Deck;
import pw.spn.blackjack.deck.impl.BlackJackDeck;
import pw.spn.blackjack.game.Game;
import pw.spn.blackjack.hand.Hand;
import pw.spn.blackjack.io.InputReader;
import pw.spn.blackjack.io.impl.ConsoleInputReader;
import pw.spn.blackjack.player.Player;
import pw.spn.blackjack.player.impl.BlackJackPlayer;

import java.util.*;

public final class ConsoleBlackJack implements Game {
    private static final int BET = 10;
    private static final int TARGET = 21;

    private final Dealer dealer;
    private final Player player;
    private final Deck deck;
    private final InputReader inputReader;

    public ConsoleBlackJack() {
        this(new BlackJackDeck(), new ConsoleInputReader());
    }

    public ConsoleBlackJack(Deck deck, InputReader inputReader) {
        Objects.requireNonNull(deck);
        Objects.requireNonNull(inputReader);

        this.deck = deck;
        this.inputReader = inputReader;
        dealer = new BlackJackDealer(this);
        player = new BlackJackPlayer(this);
    }

    @Override
    public void start() {
        message("Welcome to BlackJack!");
        while (player.getBalance() >= BET) {
            playRound();
        }
        message("Thank you for playing.");
    }

    private void playRound() {
        message("Starting new round. Your balance is " + player.getBalance());

        dealer.startNewRound();
        player.startNewRound();

        message("Taking " + BET + " from your balance for a round bet");

        message("Dealing cards");
        deal();

        List<Command> commands = player.getAvailableCommands();
        boolean isPlayerSurrendered = false;
        boolean wasDoubled = false;
        while (!commands.isEmpty()) {
            Command command = chooseCommand(commands);
            if (Surrender.class.isAssignableFrom(command.getClass())) {
                isPlayerSurrendered = true;
            }
            if (DoubleDown.class.isAssignableFrom(command.getClass())) {
                wasDoubled = true;
            }
            command.action().accept(player);
            printStatus();
            if (isPlayerSurrendered || wasDoubled) {
                break;
            }
            commands = player.getAvailableCommands();
        }
        if (!isPlayerSurrendered && !player.getHands().stream().allMatch(h -> h.getValue() > TARGET)) {
            dealer.play();
            calculateWinnings();
            message("Round is finished");
            message("Your cards:");
            player.getHands().forEach(System.out::println);
            message("Dealer cards:\n" + dealer.getHand());
        }
    }

    private void deal() {
        Hand dealerHand = dealer.getHand();
        player.hit();
        dealerHand.addCard(deck.nextCard());
        player.hit();
        dealerHand.addCard(deck.nextCard());
        printStatus();
    }

    private void printStatus() {
        message("Your cards:\n" + player.getHands().get(0));
        message("Dealer cards:\n[" + dealer.getHand().getCards().get(0) + ", *] (?)");
    }

    private Command chooseCommand(List<Command> availableCommands) {
        int[] validCommandCodes = availableCommands.stream().mapToInt(Command::getShortcut).toArray();
        Arrays.sort(validCommandCodes);
        while (true) {
            showCommands(availableCommands);
            int command = inputReader.nextChar();
            if (Arrays.binarySearch(validCommandCodes, command) >= 0) {
                return availableCommands.stream().filter(c -> c.getShortcut() == command).findFirst().get();
            }
            message("Invalid command");
        }
    }

    private void showCommands(List<Command> commands) {
        message("Choose option:");
        commands.stream().map(command -> command.getName() + " [" + command.getShortcut() + "]").forEach(this::message);
    }

    @Override
    public int getBet() {
        return BET;
    }

    @Override
    public Deck getDeck() {
        return deck;
    }

    private void calculateWinnings() {
        Set<Integer> winningHandsIndices = new HashSet<>();
        Set<Integer> drawHandsIndices = new HashSet<>();
        int dealerValue = dealer.getHand().getValue();
        for (int i = 0; i < player.getHands().size(); i++) {
            int playerValue = player.getHands().get(i).getValue();
            if (playerValue <= TARGET) {
                if (playerValue > dealerValue || dealerValue > TARGET) {
                    winningHandsIndices.add(i);
                }
                if (playerValue == dealerValue) {
                    drawHandsIndices.add(i);
                }
            }
        }
        player.payWin(winningHandsIndices);
        player.payDraw(drawHandsIndices);
    }

    private void message(String message) {
        System.out.println(message);
    }
}
