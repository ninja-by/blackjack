package pw.spn.blackjack.deck.impl;

import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.deck.Deck;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public final class StandardDeck implements Deck {
    private static final int DECK_CAPACITY = 52;

    private final List<Card> cards;
    private final Random random;

    public StandardDeck() {
        this(new SecureRandom());
    }

    public StandardDeck(Random random) {
        Objects.requireNonNull(random);

        this.random = random;
        cards = new ArrayList<>(DECK_CAPACITY);
    }

    @Override
    public Card nextCard() {
        if (cards.isEmpty()) {
            fillAndShuffle();
        }
        return cards.remove(cards.size() - 1);
    }

    private void fillAndShuffle() {
        for (Face face : Face.values()) {
            for (Suit suit : Suit.values()) {
                cards.add(new Card(face, suit));
            }
        }
        Collections.shuffle(cards, random);
    }
}
