package pw.spn.blackjack.deck.impl;

import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.deck.Deck;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public final class BlackJackDeck implements Deck {
    private static final int STANDARD_DECK_CAPACITY = 52;
    private static final int DEFAULT_NUMBER_OF_STANDARD_DECKS = 6;

    private final List<Card> cards;
    private final Random random;
    private final int numberOfStandardDecks;

    public BlackJackDeck() {
        this(DEFAULT_NUMBER_OF_STANDARD_DECKS, new SecureRandom());
    }

    public BlackJackDeck(int numberOfStandardDecks, Random random) {
        Objects.requireNonNull(random);
        if (numberOfStandardDecks <= 0) {
            throw new IllegalArgumentException("numberOfStandardDecks must be positive");
        }

        this.numberOfStandardDecks = numberOfStandardDecks;
        this.random = random;
        cards = new ArrayList<>(STANDARD_DECK_CAPACITY * numberOfStandardDecks);
    }

    @Override
    public Card nextCard() {
        if (cards.isEmpty()) {
            fillAndShuffle();
        }
        return cards.remove(cards.size() - 1);
    }

    private void fillAndShuffle() {
        for (int i = 0; i < numberOfStandardDecks; i++) {
            for (Face face : Face.values()) {
                for (Suit suit : Suit.values()) {
                    cards.add(new Card(face, suit));
                }
            }
        }
        Collections.shuffle(cards, random);
    }
}
