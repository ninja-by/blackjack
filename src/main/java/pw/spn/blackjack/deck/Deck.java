package pw.spn.blackjack.deck;

import pw.spn.blackjack.card.Card;

public interface Deck {
    Card nextCard();
}
