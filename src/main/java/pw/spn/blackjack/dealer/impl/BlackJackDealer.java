package pw.spn.blackjack.dealer.impl;

import pw.spn.blackjack.dealer.Dealer;
import pw.spn.blackjack.game.Game;
import pw.spn.blackjack.hand.Hand;
import pw.spn.blackjack.hand.impl.BlackJackHand;

public final class BlackJackDealer implements Dealer {
    private Hand hand;
    private final Game game;

    public BlackJackDealer(Game game) {
        this.game = game;
    }

    @Override
    public void startNewRound() {
        hand = new BlackJackHand();
    }

    @Override
    public Hand getHand() {
        return hand;
    }

    @Override
    public void play() {
        while (hand.getValue() < 17) {
            hand.addCard(game.getDeck().nextCard());
        }
    }
}
