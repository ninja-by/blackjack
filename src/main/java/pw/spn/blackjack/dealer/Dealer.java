package pw.spn.blackjack.dealer;

import pw.spn.blackjack.hand.Hand;

public interface Dealer {
    void startNewRound();

    Hand getHand();

    void play();
}
