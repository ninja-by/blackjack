package pw.spn.blackjack.player;

import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.hand.Hand;

import java.util.List;
import java.util.Set;

public interface Player {
    long getBalance();

    void startNewRound();

    void doubleDown();

    void hit();

    void stand();

    void split();

    void surrender();

    List<Command> getAvailableCommands();

    List<Hand> getHands();

    Hand getCurrentHand();

    void payWin(Set<Integer> winningHandsIndices);

    void payDraw(Set<Integer> drawHandsIndices);
}
