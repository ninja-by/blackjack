package pw.spn.blackjack.player.impl;

import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.command.impl.*;
import pw.spn.blackjack.game.Game;
import pw.spn.blackjack.hand.Hand;
import pw.spn.blackjack.hand.impl.BlackJackHand;
import pw.spn.blackjack.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class BlackJackPlayer implements Player {
    //commands
    private static final Command CMD_HIT = new Hit();
    private static final Command CMD_STAND = new Stand();
    private static final Command CMD_DOUBLE_DOWN = new DoubleDown();
    private static final Command CMD_SPLIT = new Split();
    private static final Command CMD_SURRENDER = new Surrender();

    // player can have up to 4 hands (3 splits)
    private final List<Hand> hands = new ArrayList<>(4);
    private final long[] betsPerHand = new long[4];
    private int currendHandIndex;
    private long balance = 100;

    private final Game game;

    public BlackJackPlayer(Game game) {
        this.game = game;
    }

    @Override
    public long getBalance() {
        return balance;
    }

    @Override
    public void startNewRound() {
        hands.clear();
        hands.add(new BlackJackHand());
        balance -= game.getBet();
        betsPerHand[0] = game.getBet();
        currendHandIndex = 0;
    }

    @Override
    public void doubleDown() {
        if (canDoubleDown()) {
            betsPerHand[0] <<= 1;
            balance -= game.getBet();
            hit();
        } else {
            throw new IllegalStateException("Can't Double Down");
        }
    }

    private boolean canDoubleDown() {
        if (isFirstMove() && balance >= game.getBet()) {
            int value = getCurrentHand().getValue();
            return value >= 9 && value <= 11;
        }
        return false;
    }

    @Override
    public void hit() {
        if (canHit()) {
            getCurrentHand().addCard(game.getDeck().nextCard());
        } else {
            throw new IllegalStateException("Can't Hit");
        }
    }

    private boolean canHit() {
        return getCurrentHand().canHit();
    }

    @Override
    public void stand() {
        if (canStand()) {
            getCurrentHand().stand();
        } else {
            throw new IllegalStateException("Can't Stand");
        }
    }

    private boolean canStand() {
        return getCurrentHand().canHit();
    }

    @Override
    public void split() {
        if (canSplit()) {
            Hand[] split = getCurrentHand().split();
            hands.remove(currendHandIndex);
            hands.add(currendHandIndex, split[0]);
            hands.add(split[1]);
            betsPerHand[hands.size() - 1] = game.getBet();
            balance -= game.getBet();
        } else {
            throw new IllegalStateException("Can't Split");
        }
    }

    private boolean canSplit() {
        if (hands.size() == 4 || balance < game.getBet()) {
            return false;
        }
        List<Card> cards = getCurrentHand().getCards();
        return cards.size() == 2 && cards.get(0).getFace() == cards.get(1).getFace();
    }

    @Override
    public void surrender() {
        if (canSurrender()) {
            getCurrentHand().stand();
            balance += game.getBet() / 2;
        } else {
            throw new IllegalStateException("Can't Surrender");
        }
    }

    private boolean canSurrender() {
        return isFirstMove() && getCurrentHand().canHit();
    }

    @Override
    public List<Command> getAvailableCommands() {
        while (true) {
            List<Command> result = new ArrayList<>();
            if (canHit()) {
                result.add(CMD_HIT);
            }
            if (canStand()) {
                result.add(CMD_STAND);
            }
            if (canDoubleDown()) {
                result.add(CMD_DOUBLE_DOWN);
            }
            if (canSplit()) {
                result.add(CMD_SPLIT);
            }
            if (canSurrender()) {
                result.add(CMD_SURRENDER);
            }
            if (result.isEmpty() && hands.size() - 1 > currendHandIndex) {
                currendHandIndex++;
                continue;
            }
            return result;
        }
    }

    @Override
    public List<Hand> getHands() {
        return hands;
    }

    @Override
    public Hand getCurrentHand() {
        return hands.get(currendHandIndex);
    }

    @Override
    public void payWin(Set<Integer> winningHandsIndices) {
        balance += sumBets(winningHandsIndices) << 1;
    }

    @Override
    public void payDraw(Set<Integer> drawHandsIndices) {
        balance += sumBets(drawHandsIndices);
    }

    private long sumBets(Set<Integer> indices) {
        return indices.stream().mapToLong(i -> betsPerHand[i]).sum();
    }

    private boolean isFirstMove() {
        return hands.size() == 1 && getCurrentHand().getCards().size() == 2;
    }
}
