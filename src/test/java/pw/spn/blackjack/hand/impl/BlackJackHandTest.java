package pw.spn.blackjack.hand.impl;

import org.junit.Test;
import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.hand.Hand;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BlackJackHandTest {
    @Test
    public void getValueWhenNoAces() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.JACK, Suit.SPADES));

        //when
        int result = hand.getValue();

        //then
        assertThat(result, is(16));
    }

    @Test
    public void getValueWithAcesWhenValueIsHigherThan10() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.JACK, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));

        //when
        int result = hand.getValue();

        //then
        assertThat(result, is(18));
    }

    @Test
    public void getValueWithOneAceWhenValueIsLowerThan10() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));

        //when
        int result = hand.getValue();

        //then
        assertThat(result, is(17));
    }

    @Test
    public void getValueWithAcesWhenValueIsLowerThan10() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));

        //when
        int result = hand.getValue();

        //then
        assertThat(result, is(18));
    }

    @Test
    public void getValueWithAcesWhenValueIsLowerThan10AndAllAcesConsideredAsOne() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SEVEN, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));
        hand.addCard(new Card(Face.THREE, Suit.DIAMONDS));

        //when
        int result = hand.getValue();

        //then
        assertThat(result, is(12));
    }

    @Test
    public void canHitIfValueLessThan21() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));

        //when
        boolean result = hand.canHit();

        //then
        assertTrue(result);
    }

    @Test
    public void cantHitIfValueMoreThan21() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        hand.addCard(new Card(Face.KING, Suit.DIAMONDS));
        hand.addCard(new Card(Face.QUEEN, Suit.DIAMONDS));

        //when
        boolean result = hand.canHit();

        //then
        assertFalse(result);
    }

    @Test
    public void cantHitIfValueIs21() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.KING, Suit.DIAMONDS));
        hand.addCard(new Card(Face.ACE, Suit.DIAMONDS));

        //when
        boolean result = hand.canHit();

        //then
        assertFalse(result);
    }

    @Test
    public void cantHitAfterStand() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.KING, Suit.DIAMONDS));
        hand.stand();

        //when
        boolean result = hand.canHit();

        //then
        assertFalse(result);
    }

    @Test
    public void split() {
        //given
        BlackJackHand hand = new BlackJackHand();
        hand.addCard(new Card(Face.KING, Suit.DIAMONDS));
        hand.addCard(new Card(Face.KING, Suit.SPADES));

        //when
        Hand[] result = hand.split();

        //then
        assertThat(result[0].getCards().size(), is(1));
        assertThat(result[0].getCards().get(0), is(new Card(Face.KING, Suit.DIAMONDS)));
        assertThat(result[1].getCards().size(), is(1));
        assertThat(result[1].getCards().get(0), is(new Card(Face.KING, Suit.SPADES)));
    }
}
