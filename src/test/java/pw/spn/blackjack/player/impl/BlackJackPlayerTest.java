package pw.spn.blackjack.player.impl;

import org.junit.Test;
import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.command.Command;
import pw.spn.blackjack.game.impl.ConsoleBlackJack;
import pw.spn.blackjack.hand.Hand;
import pw.spn.blackjack.hand.impl.BlackJackHand;
import pw.spn.blackjack.player.Player;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class BlackJackPlayerTest {
    @Test
    public void cleanUpOnNewRound() {
        //given
        Player player = createPlayer();
        player.startNewRound();
        player.hit();

        //when
        player.startNewRound();

        //then
        assertTrue(player.getHands().get(0).getCards().isEmpty());
        assertThat(player.getBalance(), is(80L));
    }

    @Test
    public void doubleDown() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.THREE, Suit.SPADES));
        hand.addCard(new Card(Face.SEVEN, Suit.SPADES));
        player.getHands().add(hand);

        //when
        player.doubleDown();

        //then
        assertThat(player.getBalance(), is(90L));
        assertThat(hand.getCards().size(), is(3));
    }

    @Test(expected = IllegalStateException.class)
    public void cantDoubleDownIfHandValueIsGreaterThan11() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.SEVEN, Suit.SPADES));
        player.getHands().add(hand);

        //when
        player.doubleDown();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantDoubleDownIfHandValueIsLowerThan9() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.THREE, Suit.SPADES));
        player.getHands().add(hand);

        //when
        player.doubleDown();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantDoubleDownIfNotTheFirstMove() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.TWO, Suit.SPADES));
        hand.addCard(new Card(Face.TWO, Suit.DIAMONDS));
        player.getHands().add(hand);

        //when
        player.doubleDown();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantDoubleDownIfLowBalance() {
        //given
        Player player = createPlayer();
        IntStream.range(0, 10).forEach(i -> player.startNewRound());
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.SIX, Suit.SPADES));
        player.getHands().add(hand);

        //when
        player.doubleDown();

        //then
        //exception is thrown
    }

    @Test
    public void hit() {
        //given
        Player player = createPlayer();
        player.startNewRound();

        //when
        player.hit();

        //then
        assertThat(player.getCurrentHand().getCards().size(), is(1));
    }

    @Test(expected = IllegalStateException.class)
    public void cantHit() {
        //given
        Player player = createPlayer();
        player.startNewRound();
        player.stand();

        //when
        player.hit();

        //then
        //exception is thrown
    }

    @Test
    public void stand() {
        //given
        Player player = createPlayer();
        player.startNewRound();

        //when
        player.stand();

        //then
        assertFalse(player.getCurrentHand().canHit());
    }

    @Test(expected = IllegalStateException.class)
    public void cantStand() {
        //given
        Player player = createPlayer();
        player.startNewRound();
        player.stand();

        //when
        player.stand();

        //then
        //exception is thrown
    }

    @Test
    public void split() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.DIAMONDS));
        player.getHands().add(hand);

        //when
        player.split();

        //then
        assertThat(player.getHands().size(), is(2));
        assertThat(player.getHands().get(0).getCards().size(), is(1));
        assertThat(player.getHands().get(0).getCards().get(0), is(new Card(Face.FIVE, Suit.SPADES)));
        assertThat(player.getHands().get(1).getCards().size(), is(1));
        assertThat(player.getHands().get(1).getCards().get(0), is(new Card(Face.FIVE, Suit.DIAMONDS)));
        assertThat(player.getBalance(), is(90L));
    }

    @Test(expected = IllegalStateException.class)
    public void cantSplitIf4Hands() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.DIAMONDS));
        player.getHands().add(hand);
        player.getHands().add(hand);
        player.getHands().add(hand);
        player.getHands().add(hand);

        //when
        player.split();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantSplitIfLowBalance() {
        //given
        Player player = createPlayer();
        IntStream.range(0, 10).forEach(i -> player.startNewRound());
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.DIAMONDS));
        player.getHands().add(hand);

        //when
        player.split();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantSplitIfMoreThan2Cards() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.DIAMONDS));
        hand.addCard(new Card(Face.FIVE, Suit.HEARTS));
        player.getHands().add(hand);

        //when
        player.split();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantSplitIfDifferentCards() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.SIX, Suit.DIAMONDS));
        player.getHands().add(hand);

        //when
        player.split();

        //then
        //exception is thrown
    }

    @Test
    public void surrender() {
        //given
        Player player = createPlayer();
        player.startNewRound();
        Hand hand = player.getCurrentHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.SIX, Suit.CLUBS));

        //when
        player.surrender();

        //then
        assertThat(player.getBalance(), is(95L));
        assertFalse(hand.canHit());
    }

    @Test(expected = IllegalStateException.class)
    public void cantSurrenderIfNotTheFirstMove() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.TWO, Suit.SPADES));
        hand.addCard(new Card(Face.TWO, Suit.DIAMONDS));
        player.getHands().add(hand);

        //when
        player.surrender();

        //then
        //exception is thrown
    }

    @Test(expected = IllegalStateException.class)
    public void cantSurrenderAfterStand() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.TWO, Suit.SPADES));
        player.getHands().add(hand);
        player.stand();

        //when
        player.surrender();

        //then
        //exception is thrown
    }

    @Test
    public void getAvailableCommands() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.HEARTS));
        player.getHands().add(hand);

        //when
        List<Command> result = player.getAvailableCommands();

        //then
        assertThat(result.size(), is(5));
    }

    @Test
    public void getAvailableCommandsForTheNextHand() {
        //given
        Player player = createPlayer();
        Hand hand = new BlackJackHand();
        hand.addCard(new Card(Face.FIVE, Suit.SPADES));
        hand.addCard(new Card(Face.FIVE, Suit.HEARTS));
        player.getHands().add(hand);
        player.split();
        player.stand();

        //when
        List<Command> result = player.getAvailableCommands();

        //then
        assertThat(result.size(), is(2));
    }

    @Test
    public void payWin() {
        //given
        Player player = createPlayer();
        player.startNewRound();

        //when
        player.payWin(Set.of(0));

        //then
        assertThat(player.getBalance(), is(110L));
    }

    @Test
    public void payDraw() {
        //given
        Player player = createPlayer();
        player.startNewRound();

        //when
        player.payDraw(Set.of(0));

        //then
        assertThat(player.getBalance(), is(100L));
    }

    private Player createPlayer() {
        return new BlackJackPlayer(new ConsoleBlackJack());
    }
}
