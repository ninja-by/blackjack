package pw.spn.blackjack.game.impl;

import org.junit.Test;
import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.deck.impl.StandardDeck;
import pw.spn.blackjack.game.Game;
import pw.spn.blackjack.io.InputReader;

import java.util.Random;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ConsoleBlackJackTest {
    @Test(timeout = 1000)
    public void simulation() {
        //given
        Game game = new ConsoleBlackJack(
                new StandardDeck(new Random(1)),
                new MockInputReader()
        );

        //when
        game.start();

        //then
        assertThat(game.getDeck().nextCard(), is(new Card(Face.TWO, Suit.HEARTS)));
    }

    private static final class MockInputReader implements InputReader {
        private final String commandsSequence = "sdhhhphhhshs";
        private int counter;

        @Override
        public int nextChar() {
            if (counter >= commandsSequence.length()) {
                return 'r';
            }
            return commandsSequence.charAt(counter++);
        }
    }
}
