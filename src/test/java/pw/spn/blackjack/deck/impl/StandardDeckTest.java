package pw.spn.blackjack.deck.impl;

import org.junit.Test;
import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;

import java.util.Random;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StandardDeckTest {
    private final StandardDeck testSubject = new StandardDeck(new Random(1));

    @Test
    public void getNextCard() {
        //given
        Card expected = new Card(Face.SIX, Suit.DIAMONDS);

        //when
        Card result = testSubject.nextCard();

        //then
        assertThat(result, is(expected));
    }
}