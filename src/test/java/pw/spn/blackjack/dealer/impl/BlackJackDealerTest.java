package pw.spn.blackjack.dealer.impl;

import org.junit.Test;
import pw.spn.blackjack.card.Card;
import pw.spn.blackjack.card.Face;
import pw.spn.blackjack.card.Suit;
import pw.spn.blackjack.deck.impl.StandardDeck;
import pw.spn.blackjack.game.impl.ConsoleBlackJack;
import pw.spn.blackjack.hand.Hand;
import pw.spn.blackjack.io.impl.ConsoleInputReader;

import java.util.Random;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BlackJackDealerTest {
    @Test
    public void cleanUpOnNewRound() {
        //given
        BlackJackDealer dealer = new BlackJackDealer(new ConsoleBlackJack());
        dealer.startNewRound();
        dealer.getHand().addCard(new Card(Face.ACE, Suit.SPADES));
        dealer.startNewRound();

        //when
        Hand result = dealer.getHand();

        //then
        assertTrue(result.getCards().isEmpty());
    }

    @Test
    public void dontHitIf17OrHigher() {
        //given
        BlackJackDealer dealer = new BlackJackDealer(new ConsoleBlackJack(new StandardDeck(new Random(3)), new ConsoleInputReader()));
        dealer.startNewRound();

        //when
        dealer.play();

        //then
        assertThat(dealer.getHand().getValue(), is(17));
    }
}
